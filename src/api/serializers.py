from api.models import Frog
from api.models import FrogEnvironment
from rest_framework import serializers
import datetime
from django.db.models import Q


class FrogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Frog
        fields = '__all__'

    def birth_death_rules(self, birthday, deathday, today):
        if deathday is not None and deathday > today or birthday > today:
                raise serializers.ValidationError("You can't predict the future. Ensure deathday and birthday are not in the future.")
        elif deathday is not None and birthday > deathday:
            raise serializers.ValidationError("Birthday cannot be later than deathday.")

    def validate(self, attrs):
        today_date = datetime.datetime.now().strftime('%Y-%m-%d')
        deathday = attrs.get('deathday')
        birthday = attrs.get('birthday')
        if not birthday:
            birthday = datetime.datetime.now().strftime('%Y-%m-%d')
        else:
            birthday = attrs.get('birthday').strftime('%Y-%m-%d')
        if deathday:
            deathday = deathday.strftime('%Y-%m-%d')
        self.birth_death_rules(birthday, deathday, today_date)

        return attrs


class FrogEnvSerializer(serializers.ModelSerializer):
    class Meta:
        model = FrogEnvironment
        fields = '__all__'

    def create(self, validated_data):
        activate = validated_data.get('activate')
        current_env = FrogEnvironment.objects.filter(activate=True)

        if activate is False and not current_env:
            activate = True

        env, created = FrogEnvironment.objects.get_or_create(name=validated_data['name'], description=validated_data['description'], activate=activate)
        return env

    def update(self, instance, validated_data):
        name = validated_data['name']
        activate = validated_data['activate']

        current_env = FrogEnvironment.objects.filter(~Q(name=name), activate=True)

        if activate is False and not current_env:
            instance.activate = True
            validated_data['activate'] = True
        elif activate is True and current_env:
            f_obj = FrogEnvironment.objects.all()
            f_obj.update(activate=False)

        super(FrogEnvSerializer, self).update(instance, validated_data)
        return instance

