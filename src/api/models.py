from django.db import models
import datetime


class Frog(models.Model):
    GENDER_TYPE = (
        ('F', 'Female'),
        ('M', 'Male'),
    )
    name = models.CharField(max_length=20, primary_key=True)
    gender = models.CharField(choices=GENDER_TYPE, max_length=1)
    birthday = models.DateField(null=False, blank=False, auto_now_add=True)
    deathday = models.DateField(null=True, blank=True)
    parent1 = models.CharField(max_length=20, null=True, blank=True)
    parent2 = models.CharField(max_length=20, null=True, blank=True)

    def age(self):
        return int((datetime.date.today() - self.birthday).days / 365.25)


class FrogEnvironment(models.Model):
    '''
    harsh = frogs become canibal, no resources and small pond, index = 0.1
    good = equal resources and big pond, index = 0.8
    average = medium resources and medium pond, index = 0.4
    '''
    name = models.CharField(max_length=100, null=False, blank=False, unique=True)
    description = models.CharField(max_length=300, null=False, blank=False)
    activate = models.BooleanField(null=False, default=False)
    index = models.FloatField(null=False, blank=False)


class Mate(models.Model):
    frog1 = models.ForeignKey(Frog, related_name='frog1_mate')
    frog2 = models.ForeignKey(Frog, related_name='frog2_mate')
    start_time = models.DateTimeField(null=False, blank=False)
    end_time = models.DateTimeField(null=True, blank=True)
    ended = models.BooleanField(default=False)
    environment = models.ForeignKey(FrogEnvironment, related_name='frog_env')
    are_eggs_hatched = models.BooleanField(default=False)


class Eggs(models.Model):
    mate_id = models.ForeignKey(Mate, related_name='mate_id')
    total_hatched = models.IntegerField(null=False, blank=False)
