from rest_framework import routers
from django.conf.urls import url
from api import views
# from api.auth import receivers


router = routers.DefaultRouter(trailing_slash=False)
router.register(r'frogs', views.FrogViewSet, base_name='frogs')
router.register(r'frogenv', views.FrogEnvViewSet, base_name='frogenv')


urlpatterns = [
    url(r'^frogs/mating$', views.mating),
    url(r'^frogs/hatching$', views.HatchingEggs.as_view())
]
urlpatterns += router.urls
