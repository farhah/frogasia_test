from django_rq import job
import random
import string
from django.utils import timezone
import time
import logging
from api.models import Mate
from api.models import Frog
from api.models import Eggs


logger = logging.getLogger(__name__)
# hndlr = logging.FileHandler(path + '/tasks.log')
# formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
# hndlr.setFormatter(formatter)
# logger.addHandler(hndlr)
# logger.setLevel(logging.DEBUG)


@job
def mate_task(frog1, frog2, frog_env):
    '''
    Can do something more like
    who's mating with who - can do analytics on frequency of mating and what contribute to it. is it the environment?
    For the sake of simplicity, random time is given as if frogs are mating.
    Note: we're micro managing those frogs and when 2 frogs are mating we put them in a place
    1 couple at a time. Therefore making a queue out of it is suitable.
    ** actual reason is I use python rq without concurrency for its rqworker (becuase I don't know how). I can use celery and rabbitmq but it's too much for a simple project. Ideal open concurrency = 100 **
    '''
    start = timezone.now()
    logger.debug('start mating task for %s %s, %s' % (frog1, frog2, start))

    range_ = int((10 / frog_env.index) * 0.3)
    random_time = random.randrange(1, range_)  # 1 is equivalent to 1 frog minute.
    time.sleep(random_time)

    end = timezone.now()
    taken = end - start
    logger.debug('done mating task for %s %s , random time is %s , total time is %s - %s = %s' % (frog1, frog2, random_time, start, end, taken))

    frog1_obj = Frog(name=frog1)
    frog2_obj = Frog(name=frog2)
    mate = Mate(frog1=frog1_obj, frog2=frog2_obj, start_time=start, end_time=end, ended=True, environment=frog_env)
    mate.save()


@job
def hatch_eggs_task(mated, frog_env):
    '''
    Do analytics based on numbers of eggs hatched, does environment contribute to a bigger population?
    For the sake of simplicity, random time is given as if eggs are incubating until hatched.
    Note: we're micro managing those eggs and only 1 group of eggs can be hatched one at a time.
    ** actual reason is I use python rq without concurrency for its rqworker (becuase I don't know how). I can use celery and rabbitmq but it's too much for a simple project. Ideal open concurrency = 100 **
    '''
    logger.debug('hatching eggs start')
    range_ = int((30 / frog_env.index) * 0.3)
    time.sleep(random.randrange(1, range_))  # 1 is equivalent to 1 frog day.
    logger.debug('done hatching eggs')

    mate = Mate.objects.get(pk=mated.id)
    mate.are_eggs_hatched = True
    mate.save(update_fields=['are_eggs_hatched'])

    lower_bound = int(50 * frog_env.index)
    upper_bound = int(300 * frog_env.index)
    total_hatched_eggs = random.randrange(lower_bound, upper_bound)

    eggs = Eggs(mate_id=mated, total_hatched=total_hatched_eggs)
    eggs.save()

    # hatched eggs contribute to overall population, insert into Frog
    # ideally I should put the task below as a subtask of this current task. But I don't know how python rq can handle that.
    list_frogs = []
    for i in range(total_hatched_eggs):
        name = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        gender = random.choice(['F', 'M'])
        frog = Frog(name=name, gender=gender, parent1=mated.frog1.name, parent2=mated.frog2.name)
        list_frogs.append(frog)

    if list_frogs:
        Frog.objects.bulk_create(list_frogs)
    logger.debug('There are %s newly created frogs' % (len(list_frogs)))


