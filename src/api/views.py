from api.serializers import FrogSerializer
from api.serializers import FrogEnvSerializer
from rest_framework import viewsets
from api.models import Frog
from api.models import Mate
from api.models import FrogEnvironment
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from api.tasks import mate_task, hatch_eggs_task
from rest_framework.decorators import list_route


def env_check():
    frog_env = FrogEnvironment.objects.filter(activate=True)
    if frog_env is None:
        return Response('Please activate an environment before you do anything else.', status=status.HTTP_400_BAD_REQUEST)
    elif len(frog_env) > 1:
        return Response('Only 1 activated environment is allowed.', status=status.HTTP_400_BAD_REQUEST)

    return frog_env[0]


class FrogViewSet(viewsets.ModelViewSet):
    serializer_class = FrogSerializer
    queryset = Frog.objects.all()

    @list_route()
    def population(self, request):
        # {"frog1": "froggy1", "frog2": "froggy2"}
        population = Frog.objects.count()
        return Response(population, status=status.HTTP_200_OK)


class FrogEnvViewSet(viewsets.ModelViewSet):
    serializer_class = FrogEnvSerializer
    queryset = FrogEnvironment.objects.all()


@api_view(['POST'])
def mating(request):
    ''' some frogs are asexual depending on their type. Assume gender doesn't play a role in frogs'''
    env = env_check()
    frog1 = request.data.get('frog1')
    frog2 = request.data.get('frog2')
    q = Frog.objects.filter(name__in=[frog1, frog2])

    if len(q) == 2:
        mate_task.delay(frog1, frog2, env)  # send to queue to avoid IO blocking
    elif frog1 == frog2:
        return Response('Both frogs must be different.'.format(frog1, frog2), status=status.HTTP_404_NOT_FOUND)
    else:
        return Response('Both frogs must exist', status=status.HTTP_404_NOT_FOUND)
    return Response(request.data, status=status.HTTP_200_OK)


class HatchingEggs(APIView):
    def get(self, request):
        env = env_check()
        mated_frogs = Mate.objects.filter(are_eggs_hatched=False, ended=True).first()
        if mated_frogs is None:
            return Response('No eggs are available to hatch', status=status.HTTP_404_NOT_FOUND)
        hatch_eggs_task.delay(mated_frogs, env)
        return Response('Eggs are put in an incubator to hatch.', status=status.HTTP_200_OK)
