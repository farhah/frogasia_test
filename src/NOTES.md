<!---
Add your comments / notes and thoughts to this doc

Any special instructions to running your code?
-->
# Get up and running:
1. Install redis (as a queue management)
2. Install python
3. Install postgresql
4. go to frogasia_test/src then `pip install -r requirements.txt`
5. go to frogasia_test/src then `python manage.py makemigrations; python manage.py migrate; python manage.py runserver`
6. start off by creating a new frog environment down below.


## Database set up

`$ createdb frog_asia_interview_test`

with 'user'='farhah' and 'password'=''


## curl http://127.0.0.1:8000/api/v1/
> {"frogs": "http://127.0.0.1:8000/api/v1/frogs", "frogenv": "http://127.0.0.1:8000/api/v1/frogenv"}


End points:

1. POST, PUT, PATCH, DELETE on http://127.0.0.1:8000/api/v1/frogenv
2. POST, PUT, PATCH, DELETE on http://127.0.0.1:8000/api/v1/frogs
3. GET on http://127.0.0.1:8000/api/v1/frogs/population
4. POST on http://127.0.0.1:8000/api/v1/frogs/mating
5. GET on http://127.0.0.1:8000/api/v1/frogs/hatching


Other end points:

Django redis queue dashboard http://127.0.0.1:8000/django-rq

But first you must create a django superuser `python manage.py createsuperuser`


## 1. Frog environment -  http://127.0.0.1:8000/api/v1/frogenv

### Get  all frog environment
> curl -X GET http://127.0.0.1:8000/api/v1/frogenv

### Get an environment
> curl -X GET http://127.0.0.1:8000/api/v1/frogenv/1

### Add a new frog environment.
> curl -X POST http://127.0.0.1:8000/api/v1/frogenv/1 -d "name=average&description=medium resources and medium pond&activate=false&index=0.4"

some example :

  {name = harsh
  description = frogs become canibal, no resources and small pond
  activate = false
  index = 0.1
  }

  {
  name = good
  description = equal resources and big pond
  activate = false
  index = 0.8
  }

  {
  name = average
  description = medium resources and medium pond
  activate = false
  index = 0.4
  }


### Edit the whole values of an environment
> curl -X PUT http://127.0.0.1:8000/api/v1/frogenv/12 -d "name=average&description=medium resources and medium pond.&activate=True&index=0.45"

### Edit some values of an environment
By default drf doesn't allow PATCH for a few fields. Need to send all fields. It acts somewhat the same as PUT. In order for PATCH to work as what it is supposed to be, partial=True needs to be enabled. http://www.django-rest-framework.org/api-guide/serializers/#partial-updates
> curl -X PATCH http://127.0.0.1:8000/api/v1/frogenv/12 -d "name=average&description=medium resources and medium pond.&activate=True&index=0.4"

### Delete an environment
> curl -X DELETE http://127.0.0.1:8000/api/v1/frogenv/12


## 2. Frogs - http://127.0.0.1:8000/api/v1/frogs

### Get all frogs
> curl -X GET http://127.0.0.1:8000/api/v1/frogs

### Get a frog
> curl -X GET http://127.0.0.1:8000/api/v1/frogs/698ypbo0roc

### Add a new frog
> curl -X POST http://127.0.0.1:8000/api/v1/frogs -d "name=68ypbo0roc&gender=F&birthday=2017-04-29&deathday=&parent1=froggy1&parent2=froggy2"

### Edit the whole value of a frog
> curl -X PUT http://127.0.0.1:8000/api/v1/frogs/68ypbo0roc -d "name=69998ypbo0roc&gender=F&birthday=2017-04-29&deathday=&parent1=froggy1&parent2=froggy2"

### Edit some values of a frog
> curl -X PATCH http://127.0.0.1:8000/api/v1/frogs/68ypbo0roc -d "name=698ypbo0roc&gender=F&birthday=2017-04-29&deathday=&parent1=froggy1&parent2=froggy2"

### Delete a frog
> curl -X DELETE http://127.0.0.1:8000/api/v1/frogs/68ypbo0roc


## 3. Frog population - http://127.0.0.1:8000/api/v1/frogs/population

### Get population
> curl -X GET http://127.0.0.1:8000/api/v1/frogs/population


## 4. Frogs mating process http://127.0.0.1:8000/api/v1/frogs/mating

### Send 2 frogs for mating (assume frog is asexual)
> curl -X POST http://127.0.0.1:8000/api/v1/frogs/mating -d "frog1=froggy1&frog2=froggy2"


## 5. Hatch frogs eggs http://127.0.0.1:8000/api/v1/frogs/hatching

### Get any unhatched eggs for hatching
> curl -X GET http://127.0.0.1:8000/api/v1/frogs/hatching

